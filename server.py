import socket
import json
import time

UDP_IP = "192.168.1.7"
UDP_PORT = 31101


class Server:
    def __init__(self):
        print("hello")

    def start(self):
        self.sock = socket.socket(socket.AF_INET,  # Internet
                                  socket.SOCK_DGRAM)  # UDP
        self.sock.bind((UDP_IP, UDP_PORT))

    def recv_file(self, filename, size, block_size, order_size):
        f = open(filename, "wb")
        received = 0
        while received < size:
            data, addr = self.sock.recvfrom(1024)  # buffer size is 1024 bytes
            print("received message: ", data[order_size:])
            f.seek((block_size - order_size) * int.from_bytes(data[:order_size], byteorder='big', signed=False))
            f.write(data[order_size:])
            received += block_size - order_size
        self.sock.close();

    def recv_head(self):
        head, addr = self.sock.recvfrom(1024)  # buffer size is 1024 bytes
        print("received head: ", head)
        head = json.loads(head)
        print("json head: ", head)
        self.recv_file(str(int(time.time())) + head['data']['filename'], head['data']['size'], head['data']['block_size'],
                       head['data']['order_size'])

if __name__ == "__main__":
    s = Server()

    while(True):
        s.start()
        s.recv_head()
