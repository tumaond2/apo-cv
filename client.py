import socket
import json
import os

MAX_FILENAME_LENGTH = 256
ORDER_SIZE = 4
BLOCK_SIZE = 1024


# server details
UDP_IP_ADDRESS = "185.86.96.6"  # TODO add ip address
UDP_PORT_NO = 31101
server_address = (UDP_IP_ADDRESS, UDP_PORT_NO)


class Client:
    def __init__(self):
        print("client init")
        self.head = dict()
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.filename = ""
        self.size = 0

    def setup_client(self, filename):
        self.head = {
            "data": {
                "block_size": BLOCK_SIZE,
                "filename": filename,
                "order_size": ORDER_SIZE,
                "size": os.path.getsize(filename),
            },
            "type": "head"
        }

        if len(filename) > MAX_FILENAME_LENGTH:
            print("Error: file name is too long")
        else:
            self.filename = filename
            self.size = os.path.getsize(filename)

        print("client setup")

    def send_json_head(self):
        json_head = json.dumps(self.head, sort_keys=True, indent=4)

        # send json head
        self.client_socket.sendto(str.encode(json_head), server_address)
        print("json head sent")

    def send_contents(self):
        packet_number = 0
        with open(self.filename, "rb") as f:
            curr_position = 0
            while curr_position < self.size:
                contents = packet_number.to_bytes(ORDER_SIZE, "big")
                if self.size - curr_position < BLOCK_SIZE - ORDER_SIZE:
                    contents += f.read(self.size - curr_position)
                else:
                    contents += f.read(BLOCK_SIZE - ORDER_SIZE)

                curr_position += BLOCK_SIZE - ORDER_SIZE  # change current starting reading position
                f.seek(curr_position, 0)  # move file pointer

                self.client_socket.sendto(contents, server_address)  # send to the server
                packet_number += 1

        print("file contents sent")

    def close_socket(self):
        self.client_socket.close()
        print("socket closed")


if __name__ == "__main__":
    client = Client()
    client.setup_client("lion.jpg")
    client.send_json_head()
    client.send_contents()
    client.close_socket()
